import java.util.*;

/*
 * This is a solution for a problem taken from here:
 * https://www.hackerrank.com/challenges/ctci-connected-cell-in-a-grid/problem
 * 
 * You can test the solution by copying it to the site and submit.
 */
public class Solution
{
	  private static boolean[][] visited;
    
      //to check all neighbors of a cell more conveniently
	  private static int[] a={1, 1,-1,-1,0, 0,1,-1};
	  private static int[] b={1,-1, 1, -1,1,-1,0, 0};
	  
	  //return the maximum of a specific region
	  public static int dfs(int i,int j, int[][] arr,int len)
	  {
	      visited[i][j] = true;    
	      len++;
	      for(int k=0;k<a.length;k++)
	      {
              //Check all neighbors
	          int x=i+a[k], y=j+b[k];
	          if(x>=0 && x<arr.length && y>=0 && y<arr[0].length
	                          && arr[x][y]==1 && !visited[x][y])
	             len = dfs(x,y,arr,len);
	      }
	      return len;
	  }
  
  public static void main(String[] args)
  {
    Scanner scan = new Scanner(System.in);
    
    int n = scan.nextInt();
    int m = scan.nextInt();
    
    visited = new boolean[n][m]; 
    
    //take input
    int[][] arr = new int[n][m];
    for(int i=0;i<n;i++)
    	for(int j=0;j<m;j++)
    		arr[i][j] = scan.nextInt();
   
    int max = 0;
    for(int i=0;i<n;i++)
    	for(int j=0;j<m;j++)    
            //If you find a new region
    		if(arr[i][j]==1 && !visited[i][j])
    		{
    			int tmp = dfs(i,j,arr,0);
    			max = max > tmp ? max : tmp;
    		}
      
    System.out.println(max);
    	
    scan.close();
  } 
}
