import java.util.Comparator;
import java.util.Scanner;
import java.util.TreeSet;
/*
 * A solution for the problem "Merge equals", taken from:
 * http://codeforces.com/contest/962/problem/D
 * 
 * To check the solution, press "Submit code" in the site, choose java as the 
 * programming language, and copy the code.
 */
public class Solution_2 {
	/*
	 * Inner class to represent pairs
	 */
	private static class Pair implements Comparable<Pair>
	{
		long x;
		long y;
		public Pair(long x,long y)
		{
			this.x=x;
			this.y=y;
		}
		@Override
		public int compareTo(Pair p) {
			if(x>p.x)
				return 1;
			if(x<p.x)
				return -1;
			if(y<p.y)
				return -1;
			if(y>p.y)
				return 1;
			return 0;
		}
		public String toString()
		{
			return "("+x+","+y+")";
		}
	}

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        
        //Data structure that holds the minimum at the top
        TreeSet<Pair> set = new TreeSet<>();
        TreeSet<Pair> pos = new TreeSet<>(new Comparator<Pair>(){
			@Override
			public int compare(Pair p1, Pair p2) {
				if(p1.y<p2.y)
					return -1;
				if(p1.y>p2.y)
					return 1;
				return 0;
			}
        });
        
        for (int i = 0; i < n; i++) 
			set.add(new Pair(in.nextInt(),i));
		
        Pair cur = set.pollFirst();
        while(!set.isEmpty()&&cur!=null)
        {
        	Pair p2 = set.pollFirst();
        	if(p2.x == cur.x)
        	{
        		set.add(new Pair(cur.x*2,p2.y));
        		cur = set.pollFirst();
        	}
        	else
        	{
        		pos.add(cur);
        		cur = p2;
        	}
        }
        pos.add(cur);
        
        System.out.println(pos.size());
        while(!pos.isEmpty())
        	System.out.print(pos.pollFirst().x+" ");
        
        in.close();
    }
}